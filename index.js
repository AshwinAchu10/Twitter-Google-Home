const express = require('express');
const bodyParser = require('body-parser');
const request = require('request');
const moment = require('moment');

const aerobitService = require('./service/aerobitService');

const server = express();
server.use(bodyParser.urlencoded({
    extended: true
}));

server.use(bodyParser.json());

function getTwentyFourHourTime(amPmString) {
    var d = new Date("1/1/2013 " + amPmString);
    return d.getHours() + ':' + d.getMinutes();
}
var medications = [];
server.post('/aerobit', (req, res) => {
    var intentName = req.body.result.metadata.intentName;
    if(intentName === 'WhenIsMyNextSchedule') {
        console.log("ONE")
      var nameSlot = req.body.result.parameters.name;
      console.log("nameSlot",nameSlot)
      
      if(nameSlot) {
        const name = nameSlot;
        aerobitService.getScheduleDetails(name, function (response) {
            if (response != "ERROR") {
                for(var i=0;i<response.length;i++){
                    var newDate = new Date();
                    var formattedDate = moment(newDate).zone('+05:30').format("YYYY-MM-DD HH:MM")
                    var incomingDate = response[i].createdDate +" " + getTwentyFourHourTime(response[i].dose.id)
                    var resolvingSentence = "";
                    if(Date.parse(formattedDate) < Date.parse(incomingDate)){
      console.log("zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz",response[i].dose.id )
                        
                        resolvingSentence = "You have a next dose at " + response[i].dose.id 
                        var dataToSend = resolvingSentence
                        return res.json({
                            speech: dataToSend,
                            displayText: dataToSend,
                            source: 'aerobit'
                        });
                    }
                }
               
            } else {
                return res.json({
                    speech: "Invalid name",
                    displayText: "Invalid name",
                    source: 'aerobit'
                });
            }
        })
      } else {
        return res.json({
            speech: 'please, tell me the name',
            displayText: 'please, tell me the name',
            source: 'aerobit'
        });
      }
    } else if(intentName === 'WhatIsMyUpcomingMedications') {
        console.log("TWO")
        
      const nameSlot = req.body.result.parameters.name;
      if (nameSlot) {
          aerobitService.getUpcomingMedicationDetails(nameSlot, function (response) {
            if (response != "ERROR") {
               console.log("callbackdata",response)
                var dataToSend = response.join();

                return res.json({
                    speech: dataToSend,
                    displayText: dataToSend,
                    source: 'aerobit'
                });
            } else {
                return res.json({
                    speech: "Invalid name",
                    displayText: "Invalid name",
                    source: 'aerobit'
                });
            }
          })
      } else {
        return res.json({
            speech: 'please, tell me the name',
            displayText: 'please, tell me the name',
            source: 'aerobit'
        });
      }

    }
    
    else if(intentName === 'WhatIsMyMedicationsForDay') {
        console.log("THREE")
        
      const nameSlot = req.body.result.parameters.name;
      if (nameSlot) {
          aerobitService.getMedicationForDayDetails(nameSlot, function (response) {
            if (response != "ERROR") {
               console.log("callbackdata",response)
                var dataToSend = response.join();

                return res.json({
                    speech: dataToSend,
                    displayText: dataToSend,
                    source: 'aerobit'
                });
            } else {
                return res.json({
                    speech: "Invalid name",
                    displayText: "Invalid name",
                    source: 'aerobit'
                });
            }
          })
      } else {
        return res.json({
            speech: 'please, tell me the name',
            displayText: 'please, tell me the name',
            source: 'aerobit'
        });
      }

    }else {
        throw new Error('Invalid intent');
    }
});

server.listen((process.env.PORT || 8000), () => {
    console.log("Server is up and running...");
});
