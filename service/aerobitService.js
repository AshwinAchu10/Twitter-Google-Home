const request = require('request');
const moment = require('moment');
var _ = require('underscore');

exports.getScheduleDetails = function(name, callback) {
    request.get(getScheduleDetailsRequest(name), function (error, response, body) {
        var parsedData = JSON.parse(body);
        var result = parsedData;
        if (result!=null) {
            callback(result);
        } else {
            callback("ERROR");
        }
    })
}

function getTwentyFourHourTime(amPmString) {
    var d = new Date("1/1/2013 " + amPmString);
    return d.getHours() + ':' + d.getMinutes();
}

function getScheduleDetailsRequest(name) {
    var newDate = new Date()
    var formattedDate = moment(newDate).locale("en").format("YYYY-MM-DD")

    var xid = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6ImFzaHdpbkBnbWFpbC5jb20iLCJpYXQiOjE1MjYwMzQyODEsImV4cCI6MTUzNDY3NDI4MX0.yyegxX5VBQVkGf1tvG7yHLyGMtqkKnb-PUv6jGSpcwE';
    var apiUrl = 'http://54.189.230.24:8000/api/medicationstatement/patientname?name='+name+'&date='+formattedDate
    var options = {
        url: apiUrl,
        headers: {
            'xid': xid
        }
    };
    return options;
}


var medications = [];
var listOfGroups = [];

exports.getUpcomingMedicationDetails = function(name, callback) {
    request.get(getUpcomingMedicationDetailsRequest(name), function (error, response, body) {
        var result = JSON.parse(body);
        if (result) {
               myTestFunction(result, function(data){
                   console.log("ASAASASAASA", data)
                callback(data);

               })

        } else {
            callback("ERROR");
        }
    });
}

function myTestFunction(result, callback){

   
    result.forEach(function (item) {

        var newDate = new Date();
        var formattedDate = moment(newDate).zone('+05:30').format("YYYY-MM-DD HH:MM")
        var incomingDate = item.createdDate +" " + getTwentyFourHourTime(item.dose.id)
        

        listOfGroups.push(new Promise((resolve, reject) => {
        request.get(getMedicationDetailsRequest(item.medicationRequest.id.medicationReference.id), function(error, response, body){
        if(error){
            return reject(error);
        }else{
            
                var result = JSON.parse(body);
                var name = result.name;
                var time = item.dose.id
                var resolvingSentence = name + " ,at " + time 
                resolve(resolvingSentence);
            }       
        });
    }))
// }
})
    Promise.all(listOfGroups)
.then((allGroups) => {
var data = _.uniq(allGroups);
callback(data);
}).catch((error) => { callback(error); });
}



function getUpcomingMedicationDetailsRequest(name) {

    var newDate = new Date();
    var formattedDate = moment(newDate).zone('+05:30').format("YYYY-MM-DD")
    
    var xid = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6ImFzaHdpbkBnbWFpbC5jb20iLCJpYXQiOjE1MjYwMzQyODEsImV4cCI6MTUzNDY3NDI4MX0.yyegxX5VBQVkGf1tvG7yHLyGMtqkKnb-PUv6jGSpcwE';
    var options = {
        url: 'http://54.189.230.24:8000/api/medicationstatement/patientname?name='+name+'&date='+formattedDate,
        headers: {
            'xid': xid
        }
    };
    return options;
}

function getMedicationDetailsRequest(medicationId) {

    medicationId = "5addd301834f8f0021fc9072";

    var xid = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6ImFzaHdpbkBnbWFpbC5jb20iLCJpYXQiOjE1MjYwMzQyODEsImV4cCI6MTUzNDY3NDI4MX0.yyegxX5VBQVkGf1tvG7yHLyGMtqkKnb-PUv6jGSpcwE';
    var options = {
        url: 'http://54.189.230.24:8000/api/medicationdispense?id='+medicationId,
        headers: {
            'xid': xid
        }
    };
    return options;
}


    exports.getMedicationForDayDetails = function(name, callback) {
        request.get(getUpcomingMedicationDetailsRequest(name), function (error, response, body) {
            var result = JSON.parse(body);
            if (result) {
                   medicationsFetch(result, function(data){
                    callback(data);
                   })
    
            } else {
                callback("ERROR");
            }
        });
    }


function medicationsFetch(result, callback){
    result.forEach(function (item) {
        listOfGroups.push(new Promise((resolve, reject) => {
        request.get(getMedicationDetailsRequest(item.medicationRequest.id.medicationReference.id), function(error, response, body){
        if(error){
            return reject(error);
        }else{
            var result = JSON.parse(body);
            var name = result.name;
            var time = item.dose.id
            var resolvingSentence = name + " ,at " + time 
            resolve(resolvingSentence);
        }
        });
    }))
    });

    Promise.all(listOfGroups)
.then((allGroups) => {
var data = _.uniq(allGroups);
callback(data);
}).catch((error) => { callback(error); });
}